class RemoveStatusesFromBug < ActiveRecord::Migration[5.2]
  def change
  	remove_column :bugs, :statuses
  end
end
