class DeleteDeveloperIdsAndQaIds < ActiveRecord::Migration[5.2]
  def change
  	remove_column :projects, :developer_ids
  	remove_column :projects, :qa_ids
  end
end
