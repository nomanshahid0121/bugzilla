class MakeUserIdNotNullable < ActiveRecord::Migration[5.2]
  def change
  	change_column :projects, :user_id, :integer, null: false
  end
end
