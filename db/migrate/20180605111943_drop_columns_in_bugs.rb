class DropColumnsInBugs < ActiveRecord::Migration[5.2]
  def change
    remove_column :bugs, :title
    remove_column :bugs, :statuses
    remove_column :bugs, :category
  end
end
