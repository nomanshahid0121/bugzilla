class MakeUserIdAcceptNullInProjectUser < ActiveRecord::Migration[5.2]
  def change
    change_column :project_users, :user_id, :integer, :null => true
  end
end
