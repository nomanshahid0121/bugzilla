class ChangeNameOfUserType < ActiveRecord::Migration[5.2]
  def change
  	rename_column :users, :user_type, :user_types
  end
end
