class MakeDeveloperIdNullableInBugs < ActiveRecord::Migration[5.2]
  def change
    change_column :bugs, :developer_id, :integer, :null => true
  end
end