class ChangeDevIdsQaIdsToJson < ActiveRecord::Migration[5.2]
  def change
  	change_column :projects, :developer_ids, :json
  	change_column :projects, :qa_ids, :json
  end
end
