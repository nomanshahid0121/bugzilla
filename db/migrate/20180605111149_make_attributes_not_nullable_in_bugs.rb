class MakeAttributesNotNullableInBugs < ActiveRecord::Migration[5.2]
  def change
    change_column :bugs, :title, :string, null: false
    change_column :bugs, :statuses, :string, null: false
    change_column :bugs, :category, :string, null: false
  end
end
