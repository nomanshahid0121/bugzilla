class RenameTypeToCatergory < ActiveRecord::Migration[5.2]
  def change
    rename_column :bugs, :type, :category
  end
end
