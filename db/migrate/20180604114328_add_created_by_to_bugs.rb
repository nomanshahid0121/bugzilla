class AddCreatedByToBugs < ActiveRecord::Migration[5.2]
  def change
    add_column :bugs, :created_by, :integer
    add_column :bugs, :developer_id, :integer
  end
end
