class ChangeAttributeTypeToTextForDevAndQaIds < ActiveRecord::Migration[5.2]
  def change
  	change_column :projects, :developer_ids, :text
  	change_column :projects, :qa_ids, :text
  end
end
