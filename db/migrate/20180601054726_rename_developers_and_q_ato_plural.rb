class RenameDevelopersAndQAtoPlural < ActiveRecord::Migration[5.2]
  def change
  	rename_column :projects, :developer_id, :developer_ids
  	rename_column :projects, :qa_id, :qa_ids
  end
end
