class ChangeDevIdsAndQaIdsToIntegerArrays < ActiveRecord::Migration[5.2]
  def change
  	change_column :projects, :developer_ids, :integer
  	change_column :projects, :qa_ids, :integer
  end
end
