class ChangeAttributesToNotNullInBugs < ActiveRecord::Migration[5.2]
  def change
    change_column :bugs, :statuses, :integer, null: false
    change_column :bugs, :category, :boolean, null: false
  end
end
