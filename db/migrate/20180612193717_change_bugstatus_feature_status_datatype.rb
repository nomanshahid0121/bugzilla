class ChangeBugstatusFeatureStatusDatatype < ActiveRecord::Migration[5.2]
  def change
  	change_column :bugs, :bug_status, :integer
  	change_column :bugs, :feature_status, :integer

  end
end
