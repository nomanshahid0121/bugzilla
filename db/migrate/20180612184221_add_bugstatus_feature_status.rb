class AddBugstatusFeatureStatus < ActiveRecord::Migration[5.2]
  def change
  	add_column :bugs, :bug_status, :string
  	add_column :bugs, :feature_status, :string
  end
end
