class ChangeStatuesToStatuses < ActiveRecord::Migration[5.2]
  def change
    rename_column :bugs, :statues, :statuses
  end
end
