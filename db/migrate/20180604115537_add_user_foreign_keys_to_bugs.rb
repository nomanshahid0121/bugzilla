class AddUserForeignKeysToBugs < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :bugs, :users, column: :created_by
    add_foreign_key :bugs, :users, column: :developer_id
  end
end
