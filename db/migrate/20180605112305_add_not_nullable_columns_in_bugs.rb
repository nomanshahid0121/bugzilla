class AddNotNullableColumnsInBugs < ActiveRecord::Migration[5.2]
  def change
    add_column :bugs, :title, :string
    add_column :bugs, :statues, :integer
    add_column :bugs, :category, :boolean
  end
end
