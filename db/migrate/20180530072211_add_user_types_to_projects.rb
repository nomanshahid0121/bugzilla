class AddUserTypesToProjects < ActiveRecord::Migration[5.2]
  def change
  	add_column :projects, :manager_id, :integer
  	add_column :projects, :developer_id, :integer
  	add_column :projects, :qa_id, :integer
  end
end
