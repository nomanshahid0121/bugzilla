Rails.application.routes.draw do
  resources :projects do
    resources :bugs do 
      get 'assign_developer', on: :member
    end
  end

  devise_for :users, controllers: { invitations: 'users/invitations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


 
  root 'home#index'
  get 'bugs/assign_developer'
  get 'users/new'
  get 'users/create'
  get 'users/update'
  get 'users/edit'
  get 'users/destroy'
  get 'users/show'
  get 'users/index'
  #devise_for :user
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

   # resources :bugs
#  devise_for :users
  #root 'projects#index'
end
