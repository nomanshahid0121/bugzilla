class UsersController < ApplicationController
  def new
  end

  def create
  @user = User.new(user_params)
  @user.id = current_user.id

  if @user.save
    redirect_to @user
  else
    render 'new'
  end      
end

private
def user_params
  params.require(:user).permit(:name, :email, :user_types)
end

  def update
  end

  def edit
  end

  def destroy
  end

  def show
  end

  def index
    @users = User.all
  end
end
