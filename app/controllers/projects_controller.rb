class ProjectsController < ApplicationController
  before_action :set_project, only: %i[edit update destroy]
  def new
    @project = Project.new
    @project_user = ProjectUser.new
    authorize @project
  end

  def create
    @project = Project.new(project_params)
    @project.manager_id = current_user.id
    authorize @project
    if current_user.user_types != "manager"
      flash[:error] = "Only managers can create projects!"
      render 'new'
    elsif Project.find_by_name(@project.name) != nil
      flash[:error] = "A project with the same name already exists!"
      render 'new'
    elsif @project.save!
      flash[:success] = "Project was successfully created!"
      redirect_to @project
    else
      render 'new'
    end
  end

  def edit
    @project = Project.find(params[:id])
    authorize @project
  end

  def update
    
    @project = Project.find(params[:id])
    authorize @project
    if Project.where.not(id: @project.id).find_by_name(@project.name).present?
      flash[:error] = "A project with the same name already exists!"
      render 'edit'
    elsif @project.update_attributes!(project_params)
      if !params[:project_user_attributes].nil?
        params[:project_user_attributes][:user_id].each do |user_id|
          @project_user = ProjectUser.new(project_user_params)
          @project_user.project_id = @project.id
          @project_user.user_id = user_id
          @project_user.save!
        end
      end
      flash[:success] = "Project successfully updated!"
      redirect_to @project
    else
      render 'edit'
    end
  end

  def show
    @project = Project.find(params[:id])
    authorize @project
    session[:project_id] = @project.id
  end

  def index
    @projects = Project.all
    authorize @projects
  end

  def destroy

    @project = Project.find(params[:id])
    authorize @project
    puts "Deleting project " + @project.id.to_s
    @project.destroy
    redirect_to action: 'index'
  end
  def set_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name, :manager_id, project_user_attributes: [:project_id, :user_id]) 
  end

  private
  def project_user_params
      params.require(:project_user_attributes).permit(:project_id, :user_id)
  end
end