class ApplicationController < ActionController::Base

	

	include Pundit
	protect_from_forgery with: :exception
	before_action :authenticate_user!
    before_action :configure_permitted_parameters, if: :devise_controller?
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

	protected
	def configure_permitted_parameters
	    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :user_types, :password, :password_confirmation, :remember_me])
	    devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :name, :email, :password, :remember_me])
	    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email, :user_types, :password, :password_confirmation, :current_password])	   
	end

	def user_not_authorized
    	flash[:warning] = 'You are not authorized to perform this action.'
    	redirect_to(request.referer || root_path)
  	end
end
