class BugsController < ApplicationController
  before_action :set_project

  def index
    @bugs = @project.bugs
    authorize @bugs
  end

  def new
    @bug = Bug.new
  end

  def create
    @bug = Bug.new(bug_params)
    @bug.project_id = session[:project_id]
    @bug.created_by = current_user.id
    
    if @bug.title.empty?
      flash[:error] = "Title field cannot be empty!"
      render 'new'
    elsif Bug.where(title: @bug.title).empty? == false
      flash[:error] = "A bug with the same name already exists!"
      render 'new'
    elsif @bug.save!
      flash[:success] = "Bug was successfully created!"
      redirect_to  project_bug_path(@project,@bug)
    else
      render 'new'
    end
  end

  def show
    @bug = Bug.find(params[:id]) 
    authorize @bug
  end

  def edit
    @bug = Bug.find(params[:id])

  end

  def update
    @bug = Bug.find(params[:id])
    authorize @bug
    if @bug.title.empty?
      flash[:error] = "Title field cannot be empty!"
      render 'edit'
    elsif Bug.where.not(id: @bug.id).where(title: @bug.title).present?
      flash[:error] = "A bug with the same name already exists!"
      render 'edit'
    elsif @bug.update_attributes!(bug_params)
      flash[:success] = "Bug successfully updated!"
      redirect_to  project_bug_path(@project,@bug)
    else
      render 'edit'
    end
  end

  def destroy
    #@bug = Bug.find(params[:id])
    @bug = Bug.find(params[:id])
    puts "Deleting bug " + @bug.id.to_s
    @bug.destroy

    flash[:success] = "Bug successfully deleted!"
    redirect_to controller: "bugs", action: 'index'


    #Bug.find(params[:id]).destroy
    #flash[:success] = "Bug successfully deleted!"
    #redirect_to controller: "projects", action: "show", id: session[:project_id]
  end

  def assign_developer
    @bug = Bug.find(params[:id])
    authorize @bug
    if current_user.user_types == "developer"
      @bug.update_attribute(:developer_id, current_user.id)
      flash[:success] = "Bug assigned to the developer!"
    else
      flash[:error] = "Only developers can be assigned to solve bugs!"
    end
    redirect_to  project_bug_path(@project,@bug)
  end

  def set_bug
    @bug = Bug.find(params[:id])
  end

  def set_project
    @project = Project.find(params[:project_id].to_i)
  end
  
  def bug_params
    params.require(:bug).permit(:title, :description, :deadline, :screenshot, :category, :bug_status, :feature_status)
  end
end
