class Project < ApplicationRecord
	
	belongs_to :manager, :class_name => "User"
	has_one :project_name, :class_name => "ProjectUser", :foreign_key => "project_id", dependent: :destroy
	has_many :bugs, foreign_key: :project_id, dependent: :destroy
	
	def self.current
    	Thread.current[:project]
  	end

  	def self.current=(project)
    	Thread.current[:project] = project
  	end

  	def self.project_manager(project, id)
  		project.where(manager_id: id)
  	end


  	def self.project_name(curr_user_id)
  		Project.where(id: ProjectUser.select(:project_id).where(user_id: curr_user_id))
  	end

end
