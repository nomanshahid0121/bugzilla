class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
		 :recoverable, :rememberable, :trackable, :validatable

  validates :email, uniqueness: true, presence: true

  enum user_types: [ :manager, :developer, :qa ]

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }
  has_many :project_manager, :class_name => "Project", :foreign_key => "manager_id", dependent: :destroy
  has_one :project_user, :class_name => "ProjectUser", :foreign_key => "user_id", dependent: :destroy
  validates_uniqueness_of :id, scope: :id
  has_many :bug_creator, :class_name => "Bug", :foreign_key => "created_by", dependent: :destroy
  has_one :bug_solver, :class_name => "Bug", :foreign_key => "developer_id", dependent: :destroy
  
  public	  


  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def developer?
    user_types == 'developer'
  end

  def manager?
    user_types == 'manager'
  end

  def qa?
    user_types == 'qa'
  end

  def display_name
    name || email
  end
  
  
  def self.users_not_selected_for_projects
			User.where.not(user_types: 0).where.not(id: ProjectUser.select(:user_id))
  end

  def self.developers_assigned_to_project(project_id)
    User.where(id: ProjectUser.where(project_id: project_id).select(:user_id)).where(user_types: 1)
  end

  def self.qas_assigned_to_project(project_id)
    User.where(id: ProjectUser.where(project_id: project_id).select(:user_id)).where(user_types: 2)
  end  


  def self.developers_not_assigned_to_project(project_id)
    User.where.not(id: ProjectUser.where(project_id: project_id).select(:user_id)).where(user_types: 1)
  end

  def self.qas_not_assigned_to_project(project_id)
    User.where.not(id: ProjectUser.where(project_id: project_id).select(:user_id)).where(user_types: 2)
  end  

  

  def self.developers_assigned_to_current_project(project_id)
    User.where(id: ProjectUser.select(:user_id).where(project_id: project_id)).where(user_types: 1)
  end

  def self.qas_assigned_to_current_project(project_id)
    User.where(id: ProjectUser.select(:user_id).where(project_id: project_id)).where(user_types: 2)
  end

end