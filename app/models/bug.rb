class Bug < ApplicationRecord

  enum bug_statuses: [ :status_new, :status_started, :status_resolved ]
  enum feature_statuses: [ :new_status, :started_status, :completed_status]
  validates :title, uniqueness: true, presence: true
  validates :category, presence: true
  
  mount_uploader :screenshot, ImageUploader

  belongs_to :project
  belongs_to :user, optional: true


  def self.current
    Thread.current[:bug]
  end

  def self.current=(bug)
    Thread.current[:bug] = bug
  end

  def assigned?
    developer_id.present?
  end

  def is_feature?
    category == 'feature'
  end

  def is_bug?
    category == 'bug'
  end

  def resolved?
    statuses == 'Resolved' || statuses == 'Completed'
  end

  def self.find_bug_creator(bug)
    User.select(:name).where(id: bug.created_by)
  end
 
  def self.find_bug_developer(bug)
    User.select(:name).where(id: bug.developer_id)
  end
end