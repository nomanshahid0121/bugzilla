$(function() {


	if(document.querySelector('input[name = "bug[category]"]:checked') == null){
		return;
	}

	if(document.querySelector('input[name = "bug[category]"]:checked').value == "true"){
						
			document.querySelector("#feature_status").style.display = "none";
			document.querySelector("#bugs_status").style.display = "inline";
	}
	else {
			document.querySelector("#bugs_status").style.display = "none";
			document.querySelector("#feature_status").style.display = "inline";
	}
})
// window.onload = function(e) {
// 	document.querySelector("#feature_status").style.display = "none";		
// }
function evaluateType() {
	if(document.querySelector('input[name = "bug[category]"]:checked').value == "true"){
						
			document.querySelector("#feature_status").style.display = "none";
			document.querySelector("#bugs_status").style.display = "inline";
	}
	else {
			document.querySelector("#bugs_status").style.display = "none";
			document.querySelector("#feature_status").style.display = "inline";
	}
}
function validateFiles(inputFile){
	var extErrorMessage = "Only: .gif or .png is allowed";
  	var allowedExtension = ["gif", "png"];
	var fileName = inputFile.value;
    if(!(new RegExp('(' + allowedExtension.join('|').replace(/\./g, '\\.') + ')$')).test(fileName)) {
    		window.alert(extErrorMessage);
    		inputFile.value = '';
    }
}