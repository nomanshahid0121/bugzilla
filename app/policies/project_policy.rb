class ProjectPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.user_types == "manager"
        scope.all
      else
        scope.where(id: ProjectUser.select(:project_id).where(user_id: user.id))
      end
    end
  end

  def destroy?
    user.manager?
  end

  def show?
    user.id == record.manager_id or !ProjectUser.where(project_id: record.id).where(user_id: user.id).empty?
  end

  def index?
  	puts is_user_manager?
  	is_user_manager? or !ProjectUser.where(user_id: user.id).empty?
  end

  def is_user_manager?
    user.user_types == "manager"
  end 

  def is_manager_of_project?
    user.id == record.manager_id
  end

  def edit?
    is_user_manager? and is_manager_of_project?
  end

  def update?
    is_user_manager?
  end

  def new?
    is_user_manager?
  end

  def create?
    is_user_manager?
  end
end
