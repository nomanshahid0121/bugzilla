class BugPolicy < ApplicationPolicy
  attr_reader :user, :bug
  def initialize(user, bug)
    @user = user
    @bug = bug
  end

  class Scope < Scope
    def resolve
       if user.user_types == "manager"
        scope.where(id: Bug.select(:id).where(project_id: Project.select(:id).where(manager_id: user.id)))
      else
        scope.where(id: Bug.select(:id).where(project_id: ProjectUser.select(:project_id).where(user_id: user.id)))
      end
    end
  end

  def index?
    can_user_access_bug?
  end
  
  def show?
    can_user_access_bug?
  end

  def can_user_access_bug?
    if user.user_types == "manager"
      !Bug.where(project_id: Project.select(:id).where(manager_id: user.id)).empty?
    else
      !Bug.where(project_id: ProjectUser.select(:project_id).where(user_id: user.id)).empty?
    end
  end



  def assign_developer?
    user.user_types == "developer"
  end




  
end